import Image from "next/image";

const ImagePage = () => (
  <div>
    <p>Car image unoptimized:</p>
    <img
      src="/images/car.png"
      alt="Car unpotimized"
      width={512}
      height={512}
    ></img>

    <p>Car image optimized using next/image:</p>
    <Image
      src="/images/car.png"
      width={512}
      height={512}
      alt="Car optimized using <Image> component"
    />
  </div>
);

export default ImagePage;
