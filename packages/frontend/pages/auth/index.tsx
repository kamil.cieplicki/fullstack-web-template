import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import Link from "next/link";
import React from "react";

type SampleData = {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
};

export default function Auth({
  data,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { userId, id, title } = data;
  return (
    <div>
      <p>userId: {userId}</p>
      <p>id: {id}</p>
      <p>title: {title}</p>
      <Link href="/">Back to Main Page</Link>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<{
  data: SampleData;
}> = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/todos/1");
  const data = await res.json();
  console.log(data);
  return {
    props: { data },
  };
};
