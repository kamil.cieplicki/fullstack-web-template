# Fullstack Web Template in dockerized monorepo with Typescript

## Approach
- Two independent packages for backend and frontend
- Dockerfile in each package
- Three containers: frontend (NextJS), backend (NestJS), postgres_db (Postgres)
- One config file `docker-compose.yml` which create 3 containers (frontend, backend, postgres_db)

## How to start
- Make sure that you have installed Docker. Enter in CLI: `docker -v` It should return version of installed Docker eg. `Docker version 20.10.20, build 9fdeb9c`
- After clone repo, you only have to run in CLI: `make dev-build` (first time) to start local development with watch.
- Next launches: `make dev-start`
- Ports:
  - backend: 4444
  - frontend: 3333
